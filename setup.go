package main

import (
	"os"
	"fmt"
	"database/sql"
	data "bitbucket.org/cornjacket/view_hndlr_4_app3_sys6/data_appnum4_appapi3_sysapi6"
)

func setupTablePackages() {
	dbPassword := os.Getenv("DB_PASSWORD")
	if dbPassword == "" {
		fmt.Printf("init\tdb Password not provided. Exiting...\n")
		os.Exit(1)
	}
	dbConnString := "root:"+dbPassword+"@tcp(127.0.0.1:3306)/test?parseTime=true"
	fmt.Printf("init\tdbConnString: %s\n", dbConnString)
	db_conn, err := sql.Open("mysql", "root:"+dbPassword+"@tcp(127.0.0.1:3306)/test?parseTime=true")

	if err != nil {
		panic(err.Error())
	}
	data.Init(db_conn)
}
