package dataproc

import (
	// Standard library packets
	"encoding/json"
	"fmt"
	"net/http"

	// Third party packages
	//"github.com/cornjacket/iot/message"
	"github.com/julienschmidt/httprouter"
)

type (
	Data struct {
		Eui	string // this should be intrinsic_id
		Ts	uint64
		ValueA	int
		ValueB	int
	}
)

// DataProcController represents the controller for the data processors
type (
	DataProcController struct {
	}
)

var verbose = false

var serviceName = "UNKNOWN"

func NewDataProcController(name string, r *httprouter.Router, NWorkers int, fp func(int, Data) bool, pathName string) *DataProcController {

	serviceName = name
	fullPath := "/" + pathName
	// Start the dispatcher.
	fmt.Printf("%s\tDATAPROC\tNEW\tINFO\tStarting the dispatcher at %s\n", serviceName, fullPath)

	StartDispatcher(NWorkers)

	dpc := DataProcController{}
	// register data handler
	registerDataProcHandler(fp) // if i want the function to be attached to the controller instance then i need to attach it to the struct
	// Post a data resource
	r.POST(fullPath, dpc.CreateData) // TODO: Something in the framework should validate that there are no duplicate paths. Not sure what happens in that case.

	return &dpc
}

// CreateData creates a new data resource
// returns the original data so that testing can confirm the data was received - this is not necessary.
func (dpc DataProcController) CreateData(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	// Stub a data to be populated from the body
	d := Data{}

	// Populate the data
	json.NewDecoder(r.Body).Decode(&d)

	// Marshal provided interface into JSON strucutre
	dj, _ := json.Marshal(d)

	work := WorkRequest{Data: d}

	// Push the work onto the queue.
	WorkQueue <- work
	//fmt.Println("Work request queued")
	if verbose {
		fmt.Printf("%s\tDATAPROC\tCREATE\tINFO\tWork Request Queued: %v\n", work)
	}

	// Write content-type, statuscode, payload
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(201)
	fmt.Fprintf(w, "%s", dj)
}

