package main

import (
	// Standard library packets
	"fmt"
	"net/http"
	"os"
	"time"

	// Third party packages
	"bitbucket.org/cornjacket/iot/clientlib/service"
	"bitbucket.org/cornjacket/view_hndlr_4_app3_sys6/controllers"
	"bitbucket.org/cornjacket/view_hndlr_4_app3_sys6/dataproc"
	"bitbucket.org/cornjacket/iot/serverlib/lastboot"
	"bitbucket.org/cornjacket/iot/serverlib/version"
	"bitbucket.org/cornjacket/iot/serverlib/shutdown"
	"github.com/julienschmidt/httprouter"
	"bitbucket.org/cornjacket/iot/message"
)

var serviceName string = "VIEW_HNDLR_4_3_6" // TODO: This should be generated via function that takes in appNum, appApi, and sysApi

// TODO: Not sure if I need this...
var appNum int = 4
var appApi int = 3
var sysApi int = 6

// TODO: The following are used in the service.Register call below. Review what their purpose is and how to remove.
// for now gloablly store the destId, destUrl, and destStatus for oprouter so we can advertise with discovery
// this should really go into models...
var destId []message.ServiceId
var destUrl []string
var destStatus []message.DestServiceStatus

func main() {

	versionMajor := 0
	versionMinor := 0
	versionPatch := 0

	fmt.Println("DISCOVERY_URL: ", os.Getenv("DISCOVERY_URL"))	// fully qualified url
	accessDiscovery := false
	discoveryEndpoint := os.Getenv("DISCOVERY_URL")
	// should test if discovery endpoint is valid, maybe not here, but somewhere
	if discoveryEndpoint != "" {
		accessDiscovery = true
		fmt.Printf("%s\tMAIN\tINFO\tDiscovery Endpoint = %s\n", serviceName, discoveryEndpoint)
	} else {
		fmt.Printf("%s\tMAIN\tINFO\tDiscovery Endpoint not provided via environment.\n", serviceName)
	}

	fmt.Println("VIEW_PORTNUM: ", os.Getenv("VIEW_PORTNUM"))
	portNum := os.Getenv("VIEW_PORTNUM")
	if portNum == "" {
		portNum = "8082" // TODO: Is any other need to use service using this port
		fmt.Printf("%s\tMAIN\tINFO\tView_Hndlr Port Num will take default: %s\n", serviceName, portNum)
	} else {
		fmt.Printf("%s\tMAIN\tINFO\tView_Hndlr Port Num provided via environment: %s\n", portNum)
	}

	checkIfProjectNeedsBuild()
	setupTablePackages()

	lastBoot := message.LastBoot(time.Now().Unix())
	serviceID := message.ViewHndlrId
	serviceVersion := message.Version{Major: versionMajor, Minor: versionMinor, Patch: versionPatch}
	serviceShutdown := message.Shutdown{Service: serviceID, MagicNumber: 1234}


	// Instantiate a new router
	r := httprouter.New()

	version.NewController(r, serviceVersion)
	shutdown.NewController(r, serviceShutdown, serviceName)
	lastboot.NewController(serviceName, r, lastBoot)
	service.Init(serviceName)

	// This portion sets up the pktproc workers, however at init time there are no init routes
	// pktproc should have a function that adds a route to the pktproc route table, wait are we adding routes or are we adding services first?

	numWorkers := 100
	// TODO: This should be a forward_init struct that gets passed into the newController function below
	// TODO: Later the appNum, appApi, and sysApi will be used for the messageQ topic.
	// TODO: This service should ask discovery for the address of the message q.
	vc := controllers.NewViewController()

	// a string or path that looks like the following: "AppNum4AppApi3SysApi6"
	dataproc.NewDataProcController(serviceName, r, numWorkers, vc.ViewHandler, "data.4.3.6") // data path for appnum 4, appapi 3, sysapi 6

	// TODO: These are outdated. I need to transform into mechanism to change the destination URL for the app data path...
	//r.POST("/oproute", orc.CreateOpRoute) // useful when there is no discovery service
	//r.GET("/oproute/:id", orc.GetOpRoute)


	// TODO: The following needs to be modified to be used with a yet to be defined new Discovery service and client lib
	if accessDiscovery == true {
		// Registering with Discovery should happen after ListenAndService but that blocks,
		// so i need to fork. But not right now.
		fmt.Printf("%s\tMAIN\tINIT\tRegistering with Discovery\n",serviceName)
		opcodeSupport := false // TODO: Remove this concept
		onboardSupport := false
		notifySupport := false
		packetSupport := true
		cardinalitySupport := false
		appNum := message.NullId
		noDataSupport := ""

		// this should check for return value
		service.Register(discoveryEndpoint, portNum, serviceID, appNum, opcodeSupport, onboardSupport, notifySupport, packetSupport, cardinalitySupport, lastBoot, noDataSupport, destId, destUrl, destStatus)
	}

	// Fire up the server
	fmt.Printf("%s\tMAIN\tINIT\tStarting server on port: %s\n",serviceName, portNum)
        if err := http.ListenAndServe(":"+portNum, r); err != nil {
		fmt.Println(err.Error())
		// notify discovery of the failure
	}

}

func checkIfProjectNeedsBuild() {
	if _, err := os.Stat("./build.json"); err == nil {
		fmt.Println("Project config has changed but build was not invoked. Run \"guild Build\"\n")
		os.Exit(1)
	}
}

