package valueb

import (
  "strconv"
)

type ValueB struct {
  value int
  updated bool
}

func New() *ValueB {
  return &ValueB{}
}

func (x *ValueB) IsUpdated() bool {
  return x.updated
}

func (x *ValueB) Get() int {
  return x.value
}

func (x *ValueB) Set(value int) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *ValueB) Load(value int) {
  x.value = value
  x.updated = false
}

func (x *ValueB) String() string {
  return "ValueB"
}

func (x *ValueB) Type() string {
  return "int"
}

func (x *ValueB) Modifiers() string {
  return "not null"
}

func (x *ValueB) QueryValue() string {
  return strconv.Itoa(x.value)
}

