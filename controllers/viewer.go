package controllers

import (
	// Standard library packets
	//"bytes"
	//"encoding/json"
	"fmt"

	// Third party packages
	//"github.com/cornjacket/iot/message"
	"bitbucket.org/cornjacket/view_hndlr_4_app3_sys6/dataproc"
	data "bitbucket.org/cornjacket/view_hndlr_4_app3_sys6/data_appnum4_appapi3_sysapi6"
)

type (
	ViewController struct{
	}
)

var serviceName string = "VIEW_HNDLR"

var verbose = false

func NewViewController() *ViewController {
	controller := &ViewController{}
	return controller
}

func (vc ViewController) ViewHandler(wID int, d dataproc.Data) bool {

	dataRow := data.Row{}
	dataRow.Eui = d.Eui
	dataRow.Ts = int64(d.Ts)
	dataRow.ValueA = d.ValueA
	dataRow.ValueB = d.ValueB

	data := data.New()
	err := data.Insert(&dataRow)

	if err != nil {
		fmt.Printf("%s\tViewHandler. data table insert failed. worker %d: Worker disabled.\n", serviceName, wID)
	} else {
		fmt.Printf("%s\tViewHandler. data table insert successed. worker %d data: %v", serviceName, wID, dataRow)
	}

	return true
}
